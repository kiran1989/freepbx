# How to install freepbx and asterisk
- This document describe how to install asterisk and freepbx in ubuntu to act as a intermediate between intel cs webrtc MCU and turbo bridge audio conference solution.

## Platform Prerequisite
1. Ubuntu 20.04
2. size AWS T3a.Large

## Install asterisk
sudo apt-get update -y \
sudo apt-get upgrade -y \
sudo apt-get install -y \
sudo apt -y install bison \
sudo apt -y install wget \
sudo apt -y install openssl \
sudo apt -y install libssl-dev \
sudo apt -y install libasound2-dev \
sudo apt -y install libc6-dev \
sudo apt -y install libxml2-dev \
sudo apt -y install libsqlite3-dev \
sudo apt -y install libnewt-dev \
sudo apt -y install libncurses5-dev \
sudo apt -y install zlib1g-dev \
sudo apt -y install gcc \
sudo apt -y install g++ \
sudo apt -y install make \
sudo apt -y install perl \
sudo apt -y install uuid-dev \
sudo apt -y install git \
sudo apt -y install subversion \
sudo apt -y install libjansson-dev \
sudo apt -y install unixodbc-dev \
sudo apt -y install unixodbc-bin \
sudo apt -y install unixodbc \
sudo apt -y install autoconf \
sudo apt -y install libedit-dev \
sudo apt -y install build-essential \
sudo apt -y install sox \
sudo apt -y install unzip \
sudo apt -y install gnupg2 \
sudo apt -y install curl

## Get asterisk source
cd /usr/src \
sudo curl -O https://downloads.asterisk.org/pub/telephony/asterisk/asterisk-18-current.tar.gz \
sudo tar xvf asterisk-18-current.tar.gz \
cd asterisk-18*/

## Configure Prerequisite
sudo ./contrib/scripts/get_mp3_source.sh \
sudo ./contrib/scripts/install_prereq install \
sudo ./configure

## Make Menu for asterisk and choose required modules
sudo make menuselect

## Compile and install asterisk
sudo make -j2 \
sudo make install \
sudo make samples \
sudo make config \
sudo ldconfig

## Configure radius for asterisk
sed -i 's";\[radius\]"\[radius\]"g' /etc/asterisk/cdr.conf

vi /etc/asterisk/cdr.conf
#add below line in end of the file
- radiuscfg => /etc/radcli/radiusclient.conf

vi /etc/asterisk/cel.conf
#add below line in end of the file
- radiuscfg => /etc/radcli/radiusclient.conf

## Adding User and group for asterisk
sudo groupadd asterisk \
sudo useradd -r -d /var/lib/asterisk -g asterisk asterisk \
sudo usermod -aG audio,dialout asterisk \
sudo chown -R asterisk.asterisk /etc/asterisk \
sudo chown -R asterisk.asterisk /var/{lib,log,spool}/asterisk \
sudo chown -R asterisk.asterisk /usr/lib/asterisk

vi /etc/default/asterisk
#uncomment the below
- AST_USER="asterisk"
- AST_GROUP="asterisk"

vi /etc/asterisk/asterisk.conf 
#uncomment the below
- runuser = asterisk ; The user to run as.
- rungroup = asterisk ; The group to run as.

## Start and enable asterisk
sudo systemctl restart asterisk \
sudo systemctl enable asterisk \
sudo systemctl status asterisk

## Disable firewall
sudo ufw disable

## Enable chan_sip in astrisx cli
sudo asterisk -rvv
module load chan_sip

# Setup Freepbx

## Install packages for freepbx
sudo apt -y install apache2 \
sudo apt -y install mariadb-server \
sudo apt -y install libapache2-mod-php \
sudo apt -y install php \
sudo apt -y install php-pear \
sudo apt -y install php-cgi \
sudo apt -y install php-common \
sudo apt -y install php-curl \
sudo apt -y install php-mbstring \
sudo apt -y install php-gd \
sudo apt -y install php-mysql \
sudo apt -y install php-bcmath \
sudo apt -y install php-zip \
sudo apt -y install php-xml \
sudo apt -y install php-imap \
sudo apt -y install php-json \
sudo apt -y install php-snmp \
sudo apt -y install nodejs \
sudo apt -y install npm \
sudo apt-get upgrade -y

## Configure php and apache2
sed -i 's/^\(User\|Group\).*/\1 asterisk/' /etc/apache2/apache2.conf \
sed -i 's/AllowOverride None/AllowOverride All/' /etc/apache2/apache2.conf \
sed -i 's/\(^upload_max_filesize = \).*/\120M/' /etc/php/7.4/cli/php.ini \
sed -i 's/\(^upload_max_filesize = \).*/\120M/' /etc/php/7.4/apache2/php.ini

vi /etc/php/7.4/apache2/php.ini #increase memory_limit to 512M

## Downlaod freepbx package
cd /usr/src \
sudo curl -O http://mirror.freepbx.org/modules/packages/freepbx/freepbx-16.0-latest.tgz \
sudo tar xvf freepbx-16.0-latest.tgz \
cd freepbx

## Install package
sudo ./install -n \
fwconsole ma install pm2

## Enable rewrite and restart apache2
sudo a2enmod rewrite \
sudo systemctl restart apache2 \
sudo systemctl status apache2

# Accessing the freepbx
http://YOUR-IP/admin/config.php


- once you completed till this point now you can access the freepbx console, Now you can refer the images inside repo to configure the freepbx settings.

# Freepbx Settings
![Freepbx settings](1-freepbx-settings.png)

# Freepbx Sip Settings
![Freepbx sip settings](2-sip-settings.png)

# Chan_pjsip Settings
![chan pjsip settings](3-chan-pjsip-settings.png)

# Chan_sip Settings
![chan sip settings](4-chan-sip-settings.png)

# Adding pjsip extension
![adding pjsip extension](5-adding-pjsip-extension.png)

# Adding conference room
![adding conference room](6-add-conference-room.png)

## Now you can test using your softphone. Next we need to deploy the api server for managing the conference between mcu and turbo bridge.