from flask import Flask
from flask import request
import mysql.connector
import json
from conference import addMcuUsertoConference,addTurboUsertoconference,unmuteMcuUser,endConference
from muteunmute import unmuteturboUser,muteturboUser
import os
port = int(os.environ.get("PORT", 5000))

db = mysql.connector.connect(
  host="localhost",
  user="root",
  password="EnfinAdmin123",
  database="rooms"
)

app = Flask(__name__)


@app.route('/')
def hello():
    return 'Enfin Asterisk Api Server'

@app.route('/initsip', methods=('GET', 'POST'))
def init_sip():
    if request.method == 'POST':
        try:
            c = request.get_json(silent=True)
            room_id = c['mcu_room_id']
            turbo_sip = c['turbo_sip_user']
            con = db.cursor()
            con.execute("SELECT * FROM `rooms` WHERE mcu_room_id = '"+room_id+"'")
            res = con.fetchall()
            db.commit()
            if res:
                for x in res:
                    return json.dumps(x)
            else:
                con = db.cursor()
                con.execute("SELECT * FROM `rooms` WHERE mcu_room_id = '' LIMIT 1")
                res = con.fetchall()
                db.commit()
                if res:
                    con = db.cursor()
                    sql = "UPDATE rooms SET mcu_room_id = '"+room_id+"', turbo_sip_user = '"+turbo_sip+"' WHERE pbx_sip_user = "+str(res[0][0])
                    con.execute(sql)
                    db.commit()
                    for x in res:
                        return json.dumps(x)
                else:
                    return ("No free pbx rooms")
        except IndexError:
            return ('exception')
    else:
        return ('error')

@app.route('/startconference', methods=('GET', 'POST'))
def start_conf():
    if request.method == 'POST':
        try:
            c = request.get_json(silent=True)
            room_id = c['mcu_room_id']
            con = db.cursor()
            con.execute("SELECT * FROM `rooms` WHERE mcu_room_id = '"+room_id+"' AND occupied='no'")
            res = con.fetchall()
            db.commit()
            if res:
                for x in res:
                    addMcuUsertoConference(x[0], x[1], x[3])
                    addTurboUsertoconference(x[0], x[1], x[3])
                    unmuteMcuUser(x[0], x[1])
                    con = db.cursor()
                    sql = "UPDATE rooms SET occupied = 'yes' WHERE pbx_sip_user = "+str(x[0])
                    con.execute(sql)
                    db.commit()
                    return json.dumps(x)
            else:
                return ("Room already occuiped")
        except IndexError:
            return ('exception')
    else:
        return ('error')

@app.route('/unmuteturbo', methods=('GET', 'POST'))
def unmute_turbo():
    if request.method == 'POST':
        try:
            c = request.get_json(silent=True)
            room_id = c['mcu_room_id']
            con = db.cursor()
            con.execute("SELECT * FROM `rooms` WHERE mcu_room_id = '"+room_id+"'")
            res = con.fetchall()
            db.commit()
            if res:
                for x in res:
                    unmuteturboUser(x[0], x[1])
                    return json.dumps(x)
            else:
                return ("Check your mcu_room_id")
        except IndexError:
            return ('exception')
    else:
        return ('error')

@app.route('/muteturbo', methods=('GET', 'POST'))
def mute_turbo():
    if request.method == 'POST':
        try:
            c = request.get_json(silent=True)
            room_id = c['mcu_room_id']
            con = db.cursor()
            con.execute("SELECT * FROM `rooms` WHERE mcu_room_id = '"+room_id+"'")
            res = con.fetchall()
            db.commit()
            if res:
                for x in res:
                    muteturboUser(x[0], x[1])
                    return json.dumps(x)
            else:
                return ("Check you mcu_room_id")
        except IndexError:
            return ('exception')
    else:
        return ('error')

@app.route('/endconference', methods=('GET', 'POST'))
def end_conf():
    if request.method == 'POST':
        try:
            c = request.get_json(silent=True)
            room_id = c['mcu_room_id']
            con = db.cursor()
            con.execute("SELECT * FROM `rooms` WHERE mcu_room_id = '"+room_id+"'")
            res = con.fetchall()
            db.commit()
            if res:
                for x in res:
                    endConference(x[1])
                    con = db.cursor()
                    sql = "UPDATE rooms SET mcu_room_id = '', turbo_sip_user = '', occupied = 'no' WHERE pbx_sip_user = "+str(x[0])
                    con.execute(sql)
                    db.commit()
                    return json.dumps(x)
            else:
                return ("room not found to end conference or already ended")
        except IndexError:
            return ('exception')
    else:
        return ('error')


if __name__ == '__main__':
        app.run(host='0.0.0.0', port=port)
