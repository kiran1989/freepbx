from asterisk.ami import AMIClient
from asterisk.ami import SimpleAction

client = AMIClient(address='127.0.0.1',port=5038)
client.login(username='admin',secret='xxxxxxx')

def unmuteturboUser(a, b):
    action = SimpleAction(
        'ConfbridgeUnmute',
        ActionID='unmute',
        Conference=b,
        Channel='SIP/turbobridge.com',
    )
    future = client.send_action(action)
    response = future.response
    print (response)

def muteturboUser(a, b):
    action = SimpleAction(
        'ConfbridgeMute',
        ActionID='mute',
        Conference=b,
        Channel='SIP/turbobridge.com',
    )
    future = client.send_action(action)
    response = future.response
    print (response)
