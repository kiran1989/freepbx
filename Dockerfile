# syntax=docker/dockerfile:1

FROM python:3.8-slim-buster

WORKDIR /app

RUN pip install flask
RUN pip install asterisk-ami
RUN pip install mysql-connector-python-rf

COPY app.py .
COPY conference.py .
COPY muteunmute.py .

CMD [ "python3", "-m" , "app", "run", "--host=0.0.0.0"]
