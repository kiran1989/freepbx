from asterisk.ami import AMIClient
from asterisk.ami import SimpleAction
import time

client = AMIClient(address='127.0.0.1',port=5038)
client.login(username='admin',secret='xxxxxxxxxxxx')

def addMcuUsertoConference(a, b, c):
    action = SimpleAction(
        'Originate',
        Channel='PJSIP/'+str(a),
        Exten=b,
        Priority=1,
        Context='from-internal',
        CallerID=a,
    )
    future = client.send_action(action)
    response = future.response
    print (response)

def addTurboUsertoconference(a, b, c):
    time.sleep(1)
    action = SimpleAction(
        'Originate',
        Channel='SIP/'+c,
        Exten=b,
        Priority=1,
        Context='from-internal',
        CallerID='turbobridge.com',
    )
    future = client.send_action(action)
    response = future.response
    print (response)

def unmuteMcuUser(a, b):
    action = SimpleAction(
        'ConfbridgeUnmute',
        ActionID='unmute',
        Conference=b,
        Channel='PJSIP/'+str(a),
    )
    future = client.send_action(action)
    response = future.response
    print (response)

def endConference(a):
    action = SimpleAction(
        'ConfbridgeKick',
        ActionID='kick',
        Conference=a,
        Channel='all',
    )
    future = client.send_action(action)
    response = future.response
    print (response)
