# How to run api server
- This document describe how to run api server to manage conference.

1. import the rooms.sql DB schema to the mysql server.
2. install the required python packages specified in packages.txt

# Steps to run app in Docker
1. install docker and docker compose
2. docker-compose up -d


## Below are the list of apis
1. (initsip) from the response use the first element that is the sip user to configure in mcu room.
```
curl --location 'http://YOUR-IP-ADDRESS:8080/initsip' --header 'Content-Type: application/json' --data '{"mcu_room_id" : "5b6db2c2e6efe962e0fbferw","turbo_sip_user": "ideagroup#-----*----#-----@turbobridge.com"}'
```

2. (startconference) call this api once the meeting is started it will add both mcu and turbo sip users into conference room.
```
curl --location 'http://YOUR-IP-ADDRESS:8080/startconference' --header 'Content-Type: application/json' --data '{"mcu_room_id" : "5b6db2c2e6efe962e0fbferw"}'
```

3. (unmuteturbo) if the turbo users need to speak and that audio need to receive in mcu you need to unmute the turbo user through this. 
```
curl --location 'http://YOUR-IP-ADDRESS:8080/unmuteturbo' --header 'Content-Type: application/json' --data '{"mcu_room_id" : "5b6db2c2e6efe962e0fbferw"}'
```
4. (muteturbo) by default turbo user will be muted means listening mode. if you unmuted through above api and if you need to mute again you can use this.
```
curl --location 'http://YOUR-IP-ADDRESS:8080/muteturbo' --header 'Content-Type: application/json' --data '{"mcu_room_id" : "5b6db2c2e6efe962e0fbferw"}'
```
5. (endconference) MANDATORY api you need to call once the meeting is finished or if you called initsip, this will release the occupied users and rooms that can be used for next meeting.
```  
curl --location 'http://YOUR-IP-ADDRESS:8080/endconference' --header 'Content-Type: application/json' --data '{"mcu_room_id" : "5b6db2c2e6efe962e0fbferw"}'
```
All the mention apis needs post request
