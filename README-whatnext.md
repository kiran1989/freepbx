# Monitoring
- You must have a good monitoring system to watch these.

1. Verify mysql service is always running
2. Verify asterisk service is always running
3. Verify apache2 service is always running
4. Clean logs from /var/log/asterisk/
5. Verify api server always running
6. automate blocking of unauthorized access in asterisk